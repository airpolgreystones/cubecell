function btoi(bytes, data_num) {
        idx = (data_num - 1) * 2;
        return (bytes[idx] << 8) + bytes[idx + 1];
}

function btosigned(val) {
        var isnegative = val & 0x80;
        var boundary = 0x100;
        var minval = -boundary;
        var mask = boundary - 1;
        return isnegative ? minval + (val & mask) : val;
}

function decodeUplink(input) {
        var incomingBytes = input.bytes;
        // Decode an uplink message from a buffer
        // (array) of bytes to an object of fields.
        var decoded = {
                'CF': {},
                'AE': {},
                'particles': {},
        };

        if (incomingBytes[0] == 0xDE && incomingBytes[1] == 0xAD) {
                version = 2;
                incomingBytes = incomingBytes.slice(2);
        } else {
                version = 1;
        }

        decoded.battery_level = btosigned(incomingBytes[0]);

        if (version == 1) {
                dataBytes = incomingBytes.slice(2);
        } else if (version == 2) {
                dataBytes = incomingBytes.slice(5);
                DHTBytes = incomingBytes.slice(1);
                h = btoi(DHTBytes, 1);
                if (h != 65535) {
                        decoded.humidity = h / 100;
                }
                t = btoi(DHTBytes, 2);
                if (t != 65535){
                        decoded.temperature = (t / 100) - 40;
                }
        }

        decoded.CF.PM1 = btoi(dataBytes, 1);
        decoded.CF.PM25 = btoi(dataBytes, 2);
        decoded.CF.PM10 = btoi(dataBytes, 3);
        decoded.AE.PM1 = btoi(dataBytes, 4);
        decoded.AE.PM25 = btoi(dataBytes, 5);
        decoded.AE.PM10 = btoi(dataBytes, 6);
        decoded.particles['0.3'] = btoi(dataBytes, 7);
        decoded.particles['0.5'] = btoi(dataBytes, 8);
        decoded.particles['1.0'] = btoi(dataBytes, 9);
        decoded.particles['2.5'] = btoi(dataBytes, 10);
        decoded.particles['5.0'] = btoi(dataBytes, 11);
        decoded.particles['10.0'] = btoi(dataBytes, 12);

        return {'data': decoded};
}

