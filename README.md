# Remote Air-Quality Monitoring with CubeCell

This repo is about using the [Heltec CubeCell](https://heltec-automation-docs.readthedocs.io/en/latest/cubecell/index.html) to power a network of
distributed, autonomous air quality monitoring stations, and having them report data back via LoRaWAN and The Things Network.

Power is to be provided by a battery, with a solar cell in circuit to maintain charge and reduce the need to visit monitoring stations.

Power requirements: 3.3VDC

| State        | Current draw      |
| -------------|------------------ |
| Transmitting | 105 000μA (105mA) |
| Receiving    |  10 000μA (10mA)  |
| Standby      |      11μA         |

# Sensors

## Particle Sensors

Particle sensors being trialled are:

### [Sharp GP2Y1014AU0F](https://github.com/sharpsensoruser/sharp-sensor-demos/wiki/Application-Guide-for-Sharp-GP2Y1014AU0F-Dust-Sensor)

Power requirements: 5VDC

| State   | Current draw      |
| --------|------------------ |
| Active  |  20 000μA (20mA)  |

Sensor does not include a fan or heater, and does not need to 'warm up' before taking readings.

Sensitivity: 1μm or larger particles.
Reporting: Particle count; no distinction between 1, 2.5, or 10μm particles.
Interface: Analogue voltage.

### Clone [DSM501A](https://publiclab.org/system/images/photos/000/003/726/original/tmp_DSM501A_Dust_Sensor630081629.pdf)

Power requirements: 5VDC

| State   | Current draw      |
| --------|------------------ |
| Active  |  90 000μA (90mA)  |

Sensor needs to run for 1 minute to stabilise readings, as the heater resistor needs the time to warm up.

Sensitivity: 1μm or larger particles.
Reporting: Particle count; no distinction between 1, 2.5, or 10μm particles.
Interface: Pulse-width-modulated voltage signal.

### [Plantower PMS7003M](https://github.com/ChoiWooYoung/PMS7003-DHTXXD_Measure-Scripts/blob/master/data%20sheet/PMS7003M%20datasheet.pdf)

Power requirements: 5VDC

| State   | Current draw      |
| --------|------------------ |
| Active  | 100 000μA (100mA) |
| Standby |     200μA         |

Sensor needs to run for 30 seconds to stabilise readings after the fan is started.

Sensitivity: 1μm or larger particles.
Reporting: Cumulative counts of 1μm, 2.5μm, and 10μm particles. The 2.5μm count includes 1μm particles, and the 10μm count includes the 2.5μm count (hence also includes 1μm particle count).
Interface: RS-232 serial.

Physical connections:

| CubeCell | PMS7003M        |
| ---------|---------------- |
| GPIO0    | SLEEP1 (PIN10)  |
| GPIO4    | TX1 (PIN9)      |
| GPIO5    | RX1 (PIN7)      |
| GPIO1    | RESET1 (PIN5)   |
| GND      | GND1 (PIN3 & 4) |
| VDD      | VCC1 (PIN1 & 2) |

## Temperature and Humidity

### [DHT-22](https://datasheetspdf.com/pdf/792211/Aosong/DHT22/1)

DHT-22 humidity & temperature sensor (a more-accurate version of the DHT-11 combined sensor)

Power requirements: 3.3-6VDC, 25mA when active.

## Wind speed

## Wind direction

# Battery

Initial battery was a MIKROE-2759 Li-Poly Battery 3.7V 190mAh. This battery was sufficient for daytime use with overcast, but resulted in less-frequent readings overnight and in the morning as it discharged regularly to around 60%.

Testing now with 18650 Samsung 3.7V 2600mAh battery.

Battery is connected using the connector on the back of the CubeCell development board, and charging is managed by the CubeCell.

# Solar Cell

Seeed Studio 0.5W solar cell - https://ie.rs-online.com/web/p/solar-panels/1793740

Stated typical current 100mA.

# Server side logic

Crontab entries for retrieving data from 7-day storage, and sorting into a JSON file with unique entries:

```bash
*/5 *  *   *   *     curl -X GET --header 'Accept: application/json' --header 'Authorization: key ttn-account-v2.VFcrNl2ba1m64MkjSvS0d-CzQTRQT-TSBsmU3NCBi3E' 'https://aqm-greystones.data.thethingsnetwork.org/api/v2/query?last=10m' | jq . >> /var/www/aqm/data-in
*/5 *  *   *   *     sleep 30 && jq -n '[inputs[]]|unique_by(.time)' /var/www/aqm/data-in > /var/www/aqm/data.json
```

See [chart.html](https://teched-creations.com/aqm/chart.html) for rudimentary charting functions.
