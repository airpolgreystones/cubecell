#include <LoRaWan_APP.h>
#include <Arduino.h>
#include "Secrets.h"

/*
   set LoraWan_RGB to Active,the RGB active in loraWan
   RGB red means sending;
   RGB purple means joined done;
   RGB blue means RxWindow1;
   RGB yellow means RxWindow2;
   RGB green means received done;
*/

/*LoraWan channelsmask, default channels 0-7*/
uint16_t userChannelsMask[6] = { 0x00FF, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000 };

/*LoraWan region, select in arduino IDE tools*/
LoRaMacRegion_t loraWanRegion = ACTIVE_REGION;

/*LoraWan Class, Class A and Class C are supported*/
DeviceClass_t  loraWanClass = LORAWAN_CLASS;

/*the application data transmission duty cycle.  value in [ms].*/
uint32_t appTxDutyCycle = 15000;

/*OTAA or ABP*/
bool overTheAirActivation = LORAWAN_NETMODE;

/*ADR enable*/
bool loraWanAdr = LORAWAN_ADR;

/* set LORAWAN_Net_Reserve ON, the node could save the network info to flash, when node reset not need to join again */
bool keepNet = LORAWAN_NET_RESERVE;

/* Indicates if the node is sending confirmed or unconfirmed messages */
bool isTxConfirmed = LORAWAN_UPLINKMODE;

/* Application port */
uint8_t appPort = 2;
/*!
  Number of trials to transmit the frame, if the LoRaMAC layer did not
  receive an acknowledgment. The MAC performs a datarate adaptation,
  according to the LoRaWAN Specification V1.0.2, chapter 18.4, according
  to the following table:

  Transmission nb | Data Rate
  ----------------|-----------
  1 (first)       | DR
  2               | DR
  3               | max(DR-1,0)
  4               | max(DR-1,0)
  5               | max(DR-2,0)
  6               | max(DR-2,0)
  7               | max(DR-3,0)
  8               | max(DR-3,0)

  Note, that if NbTrials is set to 1 or 2, the MAC will not decrease
  the datarate, in case the LoRaMAC layer did not receive an acknowledgment
*/
uint8_t confirmedNbTrials = 8;

#define debugSerial Serial

volatile int pwm1_value = 0;
volatile int pwm2_value = 0;
volatile int pwm1_rising_time = 0;
const int PWM1INTR = 0;
const int PWM2INTR = 1;

bool low_power_mode = true;

#define readings_delay_minutes 5
#define sensor_warmup_seconds  60

void setup() {
  boardInitMcu();
  debugSerial.begin(115200);

  // Give the console up to 10 seconds to start listening
  while (!debugSerial && millis() < 10000);

  debugSerial.printf("Starting\n");

#if(AT_SUPPORT)
  enableAt();
#endif
  deviceState = DEVICE_STATE_INIT;
  LoRaWAN.ifskipjoin();
}

void loop() {
  switch (deviceState) {
    case DEVICE_STATE_INIT: {
#if(AT_SUPPORT)
        getDevParam();
#endif
        printDevParam();
        LoRaWAN.init(loraWanClass, loraWanRegion);
        deviceState = DEVICE_STATE_JOIN;
        break;
      }
    case DEVICE_STATE_JOIN: {
        LoRaWAN.join();
        break;
      }
    case DEVICE_STATE_SEND: {
        prepareTxFrame( appPort );
        LoRaWAN.send();
        deviceState = DEVICE_STATE_CYCLE;
        break;
      }
    case DEVICE_STATE_CYCLE: {
        // Schedule next packet transmission
        debugSerial.printf("Delaying %d minutes before taking next reading\n", readings_delay_minutes);
        //        txDutyCycleTime = appTxDutyCycle + randr( 0, APP_TX_DUTYCYCLE_RND );
        LoRaWAN.cycle(readings_delay_minutes * 60 * 1000);
        deviceState = DEVICE_STATE_SLEEP;
        break;
      }
    case DEVICE_STATE_SLEEP: {
        LoRaWAN.sleep();
        break;
      }
    default: {
        deviceState = DEVICE_STATE_INIT;
        break;
      }
  }
}

void prepareTxFrame(uint8_t port) {
  startSensors();
  debugSerial.printf("Delaying %d seconds to allow sensors to warm up\n", sensor_warmup_seconds);
  delay(sensor_warmup_seconds * 1000);
  // Read the values
  debugSerial.printf("Sensor 1 value: %d\n", pwm1_value);
  debugSerial.printf("Sensor 2 value: %d\n", pwm2_value);
  appDataSize = 0;
  appData[appDataSize++] = pwm1_value;
  appData[appDataSize++] = pwm2_value;
  stopSensors();
}

void startSensors() {
  attachInterrupt(PWM1INTR, pwm1_rising, RISING);
}

void stopSensors() {
  detachInterrupt(PWM1INTR);
}

void pwm1_rising() {
  attachInterrupt(PWM1INTR, pwm1_falling, FALLING);
  pwm1_rising_time = millis();
}

void pwm1_falling() {
  attachInterrupt(PWM1INTR, pwm1_rising, RISING);
  pwm1_value = millis() - pwm1_rising_time;
}
