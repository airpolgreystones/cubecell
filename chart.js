var source_data = null;
var AQMChart = null;
Chart.platform.disableCSSInjection = true;

function findGetParameter(parameterName) {
	var result = null,
		tmp = [];
	var items = location.search.substr(1).split("&");
	for (var index = 0; index < items.length; index++) {
		tmp = items[index].split("=");
		if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
	}
	return result;
}

function Get(remote_URL){
	var Httpreq = new XMLHttpRequest(); // a new request
	Httpreq.open("GET",remote_URL,false);
	Httpreq.send(null);
	return Httpreq.responseText;
}

function setGetParameter(paramName, paramValue) {
    var url = window.location.href;
    var hash = location.hash;
    url = url.replace(hash, '');
    if (url.indexOf(paramName + "=") >= 0)
    {
        var prefix = url.substring(0, url.indexOf(paramName + "=")); 
        var suffix = url.substring(url.indexOf(paramName + "="));
        suffix = suffix.substring(suffix.indexOf("=") + 1);
        suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
        url = prefix + paramName + "=" + paramValue + suffix;
    }
    else
    {
    if (url.indexOf("?") < 0)
        url += "?" + paramName + "=" + paramValue;
    else
        url += "&" + paramName + "=" + paramValue;
    }
    window.location.href = url + hash;
}

function btoi(bytes, data_num) {
	idx = (data_num - 1) * 2;
	return (bytes[idx] << 8) + bytes[idx + 1];
}

function btosigned(val) {
	var isnegative = val & 0x80;
	var boundary = 0x100;
	var minval = -boundary;
	var mask = boundary - 1;
	return isnegative ? minval + (val & mask) : val;
}

function Decoder(incomingBytes, port) {
	// Decode an uplink message from a buffer
	// (array) of bytes to an object of fields.
	var decoded = {
		'CF': {},
		'AE': {},
		'particles': {},
	};

	if (incomingBytes[0] == 0xDE && incomingBytes[1] == 0xAD) {
		version = 2;
		incomingBytes = incomingBytes.slice(2);
	} else {
		version = 1;
	}

	decoded.battery_level = btosigned(incomingBytes[0]);

	if (version == 1) {
		dataBytes = incomingBytes.slice(2);
	} else if (version == 2) {
		dataBytes = incomingBytes.slice(5);
		DHTBytes = incomingBytes.slice(1);
		h = btoi(DHTBytes, 1);
		if (h != 65535) {
			decoded.humidity = h / 100;
		}
		t = btoi(DHTBytes, 2);
		if (t != 65535){
			decoded.temperature = (t / 100) - 40;
		}
	}

	decoded.CF.PM1 = btoi(dataBytes, 1);
	decoded.CF.PM25 = btoi(dataBytes, 2);
	decoded.CF.PM10 = btoi(dataBytes, 3);
	decoded.AE.PM1 = btoi(dataBytes, 4);
	decoded.AE.PM25 = btoi(dataBytes, 5);
	decoded.AE.PM10 = btoi(dataBytes, 6);
	decoded.particles['0.3'] = btoi(dataBytes, 7);
	decoded.particles['0.5'] = btoi(dataBytes, 8);
	decoded.particles['1.0'] = btoi(dataBytes, 9);
	decoded.particles['2.5'] = btoi(dataBytes, 10);
	decoded.particles['5.0'] = btoi(dataBytes, 11);
	decoded.particles['10.0'] = btoi(dataBytes, 12);

	return decoded;
}

function selectStation() {
	var stations_selector = document.getElementById("station");
//	setGetParameter('station', stations_selector.value);
	var selected_station = stations_selector.value;
	[31, 7, 4, 1].forEach(days => {
		document.getElementById('lb' + days).setAttribute('href', 'chart.html?lookback=' + days + '&station=' + selected_station);
	});

	var timestamps = [];
	var battery_levels = [];
	var humidity_levels = [];
	var temperatures = [];
	var pm_2_5_levels = [];
	var pm_1_levels = [];
	var pm_10_levels = [];

	var dataset = getSourceData();

	dataset.forEach(datum => {
		if (datum["device_id"] != selected_station) {
			return;
		}
		raw_datum = atob(datum["raw"]);
		decoded_datum = Decoder(
			raw_datum.split("").map(ch => {
				return ch.charCodeAt();
			}), 0);
		timestamps.push(datum["time"]);

		battery_levels.push(decoded_datum["battery_level"]);
		humidity_levels.push(decoded_datum["humidity"]);
		temperatures.push(decoded_datum["temperature"]);
		pm_2_5_levels.push(decoded_datum["AE"]["PM25"]);
		pm_1_levels.push(decoded_datum["AE"]["PM1"]);
		pm_10_levels.push(decoded_datum["AE"]["PM10"]);
	});
	var ctx = document.getElementById('myChart').getContext('2d');
	if (AQMChart != null) {
		AQMChart.destroy();
	}
	AQMChart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: timestamps,
			datasets: [
			{
				label: 'PM 1',
				data: pm_1_levels,
				backgroundColor: [
					'rgba(255, 64, 64, 0.2)'
				],
				borderColor: [
					'rgba(255, 64, 64, 1)'
				],
				borderWidth: 1,
				yAxisID: 'pm-axis'
			},
			{
				label: 'PM 2.5',
				data: pm_2_5_levels,
				backgroundColor: [
					'rgba(255, 191, 0, 0.2)'
				],
				borderColor: [
					'rgba(255, 191, 0, 1)'
				],
				borderWidth: 1,
				yAxisID: 'pm-axis'
			},
			{
				label: 'PM 10',
				data: pm_10_levels,
				backgroundColor: [
					'rgba(64, 64, 255, 0.2)'
				],
				borderColor: [
					'rgba(64, 64, 255, 1)'
				],
				borderWidth: 1,
				yAxisID: 'pm-axis'
			},
			{
				label: 'Humidity',
				data: humidity_levels,
				type: 'line',
				backgroundColor: [
					'rgba(0, 0, 255, 0.2)'
				],
				borderColor: [
					'rgba(0, 0, 255, 1)'
				],
				borderWidth: 1,
				yAxisID: 'percent-axis'
			},
			{
				label: 'Temperature',
				data: temperatures,
				type: 'line',
				backgroundColor: [
					'rgba(255, 0, 0, 0.2)'
				],
				borderColor: [
					'rgba(255, 0, 0, 1)'
				],
				borderWidth: 1,
				yAxisID: 'temperature-axis'
			},
			{
				label: 'Battery Levels',
				data: battery_levels,
				type: 'line',
				backgroundColor: [
					'rgba(64, 64, 64, 0.2)'
				],
				borderColor: [
					'rgba(64, 64, 64, 1)'
				],
				borderWidth: 1,
				yAxisID: 'percent-axis'
			}
			]
		},
		options: {
			scales: {
				yAxes: [
				{
					id: 'pm-axis',
					position: 'right',
					ticks: {
						beginAtZero: true,
						max: 150
					}
				},
				{
					id: 'percent-axis',
					ticks: {
						beginAtZero: true,
						max: 100
					}
				},
				{
					id: 'temperature-axis',
					ticks: {
						beginAtZero: true,
						max: 50
					}
				}
				],
				xAxes: [{
					type: 'time',
					time: {
						unit: 'hour',
						stepSize: 3,
						displayFormats: {
							hour: 'dd DD/MM HH:mm'
						}
					}
				}]
			},
			layout: {
				padding: 10
			},
			responsive: true,
			maintainAspectRatio: false,
			tooltips: {
				callbacks: {
					label: function(tooltipItem, data) {
						var label = data.datasets[tooltipItem.datasetIndex].label || '';

						if (label) {
							label += ': ';
						}
						label += Math.round(tooltipItem.yLabel * 10) / 10;
						return label;
					}
				}
			}
		}
	});
}

function getStartDate() {
	var lookback = findGetParameter('lookback');
	if (lookback == null) {
		lookback = 4;
	}

	var start_date = new Date();
	start_date.setDate(start_date.getDate() - lookback + 1);
	start_date.setHours(0, 0);
	return start_date;
}

function getSourceData() {
	if (source_data == null) {
		var start_date = getStartDate();
		source_data = JSON.parse(Get("data.json")).filter(datum => {
			timestamp = new Date(datum["time"]);
			if (timestamp < start_date) {
				return false;
			}
			return true;
		});
	}
	return source_data;
}

function getStations(dataset) {
	var stations_set = new Set();
	dataset.forEach(datum => stations_set.add(datum["device_id"]));

	return Array.from(stations_set).sort();
}

getSourceData();

document.addEventListener('DOMContentLoaded', (event) => {
	var stations = getStations(getSourceData());
	var stations_selector = document.getElementById("station");
	stations.forEach(station => {
		var option = document.createElement("option");
		option.text = station;
		stations_selector.add(option);
	});

	var selected_station = findGetParameter('station');
	if (selected_station == null || !stations.includes(selected_station)) {
		selected_station = stations.values().next()["value"];
	}

	stations_selector.value = selected_station;
	stations_selector.addEventListener("change", selectStation);

	selectStation();
});
