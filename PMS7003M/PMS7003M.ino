#include <DHT_U.h>
#include <DHT.h>

#include <LoRaWan_APP.h>
#include <Arduino.h>
#include "Secrets.h"
#include <softSerial.h>

//#define NO_DHT true

/*
   set LoraWan_RGB to Active,the RGB active in loraWan
   RGB red means sending;
   RGB purple means joined done;
   RGB blue means RxWindow1;
   RGB yellow means RxWindow2;
   RGB green means received done;
*/

/*LoraWan channelsmask, default channels 0-7*/
uint16_t userChannelsMask[6] = { 0x00FF, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000 };

/*LoraWan region, select in arduino IDE tools*/
LoRaMacRegion_t loraWanRegion = ACTIVE_REGION;

/*LoraWan Class, Class A and Class C are supported*/
DeviceClass_t  loraWanClass = LORAWAN_CLASS;

/*the application data transmission duty cycle.  value in [ms].*/
uint32_t appTxDutyCycle = 15000;

/*OTAA or ABP*/
bool overTheAirActivation = LORAWAN_NETMODE;

/*ADR enable*/
bool loraWanAdr = LORAWAN_ADR;

/* set LORAWAN_Net_Reserve ON, the node could save the network info to flash, when node reset not need to join again */
bool keepNet = LORAWAN_NET_RESERVE;

/* Indicates if the node is sending confirmed or unconfirmed messages */
bool isTxConfirmed = LORAWAN_UPLINKMODE;

/* Application port */
uint8_t appPort = 2;
/*!
  Number of trials to transmit the frame, if the LoRaMAC layer did not
  receive an acknowledgment. The MAC performs a datarate adaptation,
  according to the LoRaWAN Specification V1.0.2, chapter 18.4, according
  to the following table:

  Transmission nb | Data Rate
  ----------------|-----------
  1 (first)       | DR
  2               | DR
  3               | max(DR-1,0)
  4               | max(DR-1,0)
  5               | max(DR-2,0)
  6               | max(DR-2,0)
  7               | max(DR-3,0)
  8               | max(DR-3,0)

  Note, that if NbTrials is set to 1 or 2, the MAC will not decrease
  the datarate, in case the LoRaMAC layer did not receive an acknowledgment
*/
uint8_t confirmedNbTrials = 8;

#define PMS7003_SLEEP_PIN GPIO0
#define PMS7003_RESET_PIN GPIO1
#define PMS7003_TX_PIN    GPIO5
#define PMS7003_RX_PIN    GPIO4

#define DHT22_SIGNAL_PIN  GPIO2

#define frame_data_length 26
#define frame_check_length 2
const int valid_header_data_sum = 0x42 + 0x4d + frame_data_length + frame_check_length;
#define max_tries_before_resetting 3

#define BATT_CEIL 3700
#define BATT_FLOOR 3000

#define BATT_DELAY_GRADIENTS 6
static const uint8_t battery_delay_threshold[BATT_DELAY_GRADIENTS] =
{  55,  60,  65,  70,  80,  95 };
static const uint8_t battery_delay_extra_minutes[BATT_DELAY_GRADIENTS] =
{ 355, 175,  115,  55,  25,  10 };

#define debugSerial Serial

softSerial PMSSerial(PMS7003_RX_PIN, PMS7003_TX_PIN);

#define readings_delay_seconds 300
#define sensor_warmup_seconds  30
#define serial_timeout_seconds 5

#define MAX_DHT_READS 5

bool debug_serial_connected = false;
bool deadman_expired = false;

static TimerEvent_t serial_deadman;

DHT dht(DHT22_SIGNAL_PIN, DHT22);

void log(const char *message...) {
  if (!debug_serial_connected) {
    debugSerial.begin(115200);
    delay(2000);
    debug_serial_connected = true;
  }

  if (debug_serial_connected) {
    uint32_t now = TimerGetCurrentTime();

    va_list argp;
    va_start(argp, message);
    char formatted_message[255];
    vsprintf(formatted_message, message, argp);
    va_end(argp);

    debugSerial.printf("%d %s\n", now, formatted_message);
  }
}

void setup() {
  boardInitMcu();
  TimerInit( &serial_deadman, on_serial_timeout );

  pinMode(Vext, OUTPUT);
  digitalWrite(Vext, LOW);

  pinMode(PMS7003_RESET_PIN, OUTPUT);
  pinMode(PMS7003_SLEEP_PIN, OUTPUT);
  digitalWrite(PMS7003_RESET_PIN, HIGH);
  digitalWrite(PMS7003_SLEEP_PIN, HIGH);
  PMSSerial.begin(9600);
  dht.begin();

  log("Starting");

  enableAt();
  deviceState = DEVICE_STATE_INIT;
  LoRaWAN.ifskipjoin();
}

void loop() {
  switch (deviceState) {
    case DEVICE_STATE_INIT: {
        getDevParam();
        printDevParam();
        LoRaWAN.init(loraWanClass, loraWanRegion);
        deviceState = DEVICE_STATE_JOIN;
        break;
      }
    case DEVICE_STATE_JOIN: {
        LoRaWAN.join();
        break;
      }
    case DEVICE_STATE_SEND: {
        prepareTxFrame( appPort );
        LoRaWAN.send();
        deviceState = DEVICE_STATE_CYCLE;
        break;
      }
    case DEVICE_STATE_CYCLE: {
        // Schedule next packet transmission
        int cycle_delay_seconds = readings_delay_seconds - sensor_warmup_seconds;

        uint8_t battery_level = getBatteryLevel();
        for (uint8_t delay_threshold_index = 0; delay_threshold_index < BATT_DELAY_GRADIENTS; delay_threshold_index++) {
          if (battery_level < battery_delay_threshold[delay_threshold_index]) {
            log("Battery level below %d%%; adding %d minutes to the wakeup delay", battery_delay_threshold[delay_threshold_index], battery_delay_extra_minutes[delay_threshold_index]);
            cycle_delay_seconds = cycle_delay_seconds + battery_delay_extra_minutes[delay_threshold_index] * 60;
            delay_threshold_index = BATT_DELAY_GRADIENTS;
          }
        }

        log("Delaying %d seconds before taking next reading", cycle_delay_seconds);
        LoRaWAN.cycle(cycle_delay_seconds * 1000 + randr( 0, APP_TX_DUTYCYCLE_RND ));
        delay(1000);
        debugSerial.flush();
        debugSerial.end();
        debug_serial_connected = false;

        deviceState = DEVICE_STATE_SLEEP;
        break;
      }
    case DEVICE_STATE_SLEEP: {
        LoRaWAN.sleep();
        break;
      }
    default: {
        deviceState = DEVICE_STATE_INIT;
        break;
      }
  }
}

void on_serial_timeout() {
  deadman_expired = true;
  log("Restarting sensors - deadman triggered");
  stopSensors();
  startSensors();
}

uint8_t blockingPMSRead() {
  int data = PMSSerial.read();
  while (data == -1) {
    data = PMSSerial.read();
  }
  return data;
}

void synchroniseFrame() {
  log("Synchronising to start of next frame with %ds timeout", serial_timeout_seconds);
  int frame_length = 0;
  while (frame_length != (frame_data_length + frame_check_length)) {
    if (deadman_expired) {
      return;
    }
    uint8_t synchLow = 0;
    while (synchLow != 0x4d) {
      uint8_t synchHigh = blockingPMSRead();
      while (synchHigh != 0x42) {
        synchHigh = blockingPMSRead();
      }
      synchLow = blockingPMSRead();
    }
    log("Header read.");

    frame_length = makeWord(blockingPMSRead(), blockingPMSRead());
    log("Frame length of %d uint8_ts received.", frame_length);
  }
}

byte * getPM25Data() {
  static byte incoming_data[frame_data_length];
  int data_sum = 0;
  int checksum = 0;
  while ((data_sum + valid_header_data_sum) != checksum) {
    if (deadman_expired) {
      break;
    }
    data_sum = 0;
    for (int data_offset = 0; data_offset < frame_data_length; ) {
      log("Data%d byte %d read. ", (data_offset / 2) + 1, (data_offset % 2) + 1);
      int sensor_byte = blockingPMSRead();
      incoming_data[data_offset++] = sensor_byte;
      data_sum += sensor_byte;
    }

    checksum = makeWord(blockingPMSRead(), blockingPMSRead());
    log("Checksum %d read; data sum %d; calculated checksum %d", checksum, data_sum, data_sum + valid_header_data_sum);
  }

  return incoming_data;
}

byte * getHTData() {
  static byte packed_DHT_data[4];
#ifdef NO_DHT
  packed_DHT_data[0] = 255;
  packed_DHT_data[1] = 255;
  packed_DHT_data[2] = 255;
  packed_DHT_data[3] = 255;

  return packed_DHT_data;
#endif

  bool valid_read = false;
  uint8_t read_attempts = 0;

  while (!valid_read) {
    if (deadman_expired) {
      break;
    }
    read_attempts++;
    
    // Reading temperature or humidity takes about 250 milliseconds!
    // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
    float humidity = dht.readHumidity();
    // Read temperature as Celsius (the default)
    float temperature = dht.readTemperature();

    if (isnan(humidity) || isnan(temperature)) {
      Serial.println(F("Failed to read from DHT sensor!"));
      if (read_attempts > MAX_DHT_READS) {
        break;
      }
      valid_read = false;
      delay(2000);
    } else {
      valid_read = true;

      uint16_t h = humidity * 100;
      uint16_t t = (temperature + 40) * 100;

      Serial.print(F("Humidity: "));
      Serial.print(h / 100);
      Serial.print(F("%  Temperature: "));
      Serial.print(t / 100 - 40);
      Serial.println(F("°C "));

      packed_DHT_data[0] = h >> 8;
      packed_DHT_data[1] = h;
      packed_DHT_data[2] = t >> 8;
      packed_DHT_data[3] = t;
    }

  }
  return packed_DHT_data;
}

uint8_t getBatteryLevel() {
  float level = ((float)(getBatteryVoltage() - BATT_FLOOR)) / ((float)(BATT_CEIL - BATT_FLOOR)) * 100.0f;
  if (level > 100.0f) {
    return 100;
  } else if (level < 0.0f) {
    return 0;
  } else {
    return level;
  }
}

void prepareTxFrame(uint8_t port) {
  deadman_expired = true;
  while (deadman_expired) {
    deadman_expired = false;

    appDataSize = 0;
    appData[appDataSize++] = 0xDE;
    appData[appDataSize++] = 0xAD;

    uint8_t battery_level = getBatteryLevel();
    log("Battery voltage %d gives level: %d", getBatteryVoltage(), battery_level);
    appData[appDataSize++] = battery_level;

    startSensors();

    byte *humidity_temperature_data = getHTData();
    for (int ht_byte = 0; ht_byte < 4; ht_byte++) {
      appData[appDataSize++] = humidity_temperature_data[ht_byte];
    }

    TimerSetValue(&serial_deadman, serial_timeout_seconds * 1000);
    TimerStart(&serial_deadman);

    synchroniseFrame();
    if (deadman_expired) {
      break;
    }

    byte *sensor_data = getPM25Data();
    for (int data_offset = 0; data_offset < frame_data_length; data_offset++) {
      appData[appDataSize++] = *(sensor_data + data_offset);
    }

    TimerStop(&serial_deadman);
    stopSensors();
  }
}

void startSensors() {
  digitalWrite(Vext, LOW);
  digitalWrite(PMS7003_RESET_PIN, HIGH);
  digitalWrite(PMS7003_SLEEP_PIN, HIGH);
  log("Delaying %d seconds to allow sensors to warm up.", sensor_warmup_seconds);
  delay(sensor_warmup_seconds * 1000);
}

void stopSensors() {
  digitalWrite(Vext, HIGH);
  digitalWrite(PMS7003_RESET_PIN, LOW);
  digitalWrite(PMS7003_SLEEP_PIN, LOW);
  log("Delaying %d seconds to make sure sensors switch off properly.", 5);
  delay(5 * 1000);
}
